﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Test_Auth.Data.Migrations
{
    public partial class AddTestUserArticles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "userArticles",
                columns: table => new
                {
                    ArticleKey = table.Column<string>(nullable: false),
                    UserKey = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_userArticles", x => x.ArticleKey);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "userArticles");
        }
    }
}
