﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Test_Auth.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Test_Auth.Data;
using Test_Auth.Models.HomeViewModels;

namespace Test_Auth.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext _context;
        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            var articles = _context.Articles.ToList();
            articles = articles.OrderByDescending(a => a.Date).ToList();
            var model = new HomeIndexViewModel
            {
                Articles = articles
            };
            return View(model);
        }

        public IActionResult Test()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
