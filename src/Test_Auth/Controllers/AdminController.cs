﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Test_Auth.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Test_Auth.Services;
using Microsoft.Extensions.Logging;
using Test_Auth.Data;
using System.Text;
using Test_Auth.Models.ManageViewModels;
using Microsoft.AspNetCore.Authorization;
using Test_Auth.Models.AdminViewModels;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Test_Auth.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IArticleManager _articleManager;
        public AdminController(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IArticleManager articleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _articleManager = articleManager;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetBodyContent()
        {
            var users = new Dictionary<ApplicationUser, string>();
            var roles = new Dictionary<IdentityRole, int>();
            foreach (var user in _userManager.Users)
            {
                var rolesToStr = new StringBuilder();
                foreach (var role in _roleManager.Roles)
                {

                    if (await _userManager.IsInRoleAsync(user, role.Name))
                    {
                        rolesToStr.Append(role.Name + ", ");
                    }

                }
                if (rolesToStr.Length > 0)
                    rolesToStr.Remove(rolesToStr.Length - 2, 2);
                users.Add(user, rolesToStr.ToString());
            }
            foreach (var role in _roleManager.Roles)
            {
                var usersCount = 0;
                foreach (var user in _userManager.Users)
                {
                    if (await _userManager.IsInRoleAsync(user, role.Name))
                    {
                        usersCount++;
                    }
                }
                roles.Add(role, usersCount);
            }
            var model = new AdminIndexViewModel
            {
                Users = users,
                Roles = roles
            };
            return Json(model);
        }

        [HttpGet]
        public async Task<IActionResult> EditUser(string user)
        {
            var model = new EditUserViewModel
            {
                User = new ApplicationUser(),
                UserRolesChecked = new Dictionary<string, string>()
            };
            if (isNull(user))
                return Json(model);
            var _user = await _userManager.FindByEmailAsync(user);
            var userRolesChecked = new Dictionary<string, string>();
            foreach (var role in _roleManager.Roles)
            {
                var isInRole = await _userManager.IsInRoleAsync(_user, role.Name) ? "checked" : "";
                userRolesChecked.Add(role.Name, isInRole);
            }
            model = new EditUserViewModel
            {
                User = _user,
                UserRolesChecked = userRolesChecked
            };
            return Json(model);
        }

        [HttpGet]
        public async Task<IActionResult> RemoveUser(string user)
        {
            try
            {
                var userToDel = await _userManager.FindByNameAsync(user);
                var userArticles = await _articleManager.GetUserArticles(userToDel);
                foreach (var article in userArticles)
                {
                    await _articleManager.DeleteArticle(article);
                }
                await _userManager.DeleteAsync(userToDel);
                var model = new {
                    Result = "ok"
                };
                return Json(model);
            }
            catch (Exception e)
            {
                return Json(new {
                    Result = "bad",
                    Err = e.InnerException
                });
            }
        }

        [HttpPost]
        public async Task<string> ChangeUser(string newEmail, string oldEmail, Dictionary<string, bool> privelegies)
        {
            try
            {
                var user = await _userManager.FindByNameAsync(oldEmail);
                
                if (newEmail != oldEmail)
                    await ChangeEmail(user, newEmail);      
                foreach (var role in privelegies)
                {
                    var isInRole = await _userManager.IsInRoleAsync(user, role.Key);
                    if (isInRole != role.Value)
                    {
                        if (isInRole)
                            await _userManager.RemoveFromRoleAsync(user, role.Key);
                        else
                            await _userManager.AddToRoleAsync(user, role.Key);
                    }
                }
                return "ok";
            }
            catch
            {
                return "bad";
            }
        }

        public async Task ChangeEmail(ApplicationUser user, string newEmail)
        {
            var code = await _userManager.GenerateChangeEmailTokenAsync(user, newEmail);
            await _userManager.ChangeEmailAsync(user, newEmail, code);
        }
        public bool isNull(object obj)
        {
            return (obj == null ? true : false);
        }
    }
}
