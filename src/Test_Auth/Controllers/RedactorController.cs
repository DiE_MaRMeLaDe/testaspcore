using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Test_Auth.Models.RedactorViewModels;
using Microsoft.AspNetCore.Identity;
using Test_Auth.Models;
using Test_Auth.Data;
using Microsoft.AspNetCore.Authorization;
using Test_Auth.Services;

namespace Test_Auth.Controllers
{
    [Authorize]
    public class RedactorController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IArticleManager _articleManager;
        private ApplicationUser _currentUser;
        public RedactorController(UserManager<ApplicationUser> userManager,
            IArticleManager articleManager)
        {
            _userManager = userManager;
            _articleManager = articleManager;
        }
        public IActionResult Index()
        {
            _currentUser = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var model = new RedactorIndexViewModel
            {
                Article =  new Article
                {
                    UserName = User.Identity.Name
                }
            };
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Index(RedactorIndexViewModel model)
        {
            _currentUser = _userManager.FindByNameAsync(User.Identity.Name).Result;
            if (await _articleManager.CreateArticle(_currentUser, model.Article) == 0)
            {
                ViewBag.MessageDanger = "���������� ��������� ��� �����!";
                return View(model);
            }
            else
            {
                ViewBag.MessageSuccess = "���� ������ ������� ���������!";
                return View();
            }
        }
    }
}