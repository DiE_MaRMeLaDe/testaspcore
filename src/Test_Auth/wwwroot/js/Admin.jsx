﻿//import messages
var createMessage = new function () { };
$(document).ready(function () {
    $.getScript("/js/Messages.js", function () {
        CreateMessage("Script loaded but not necessarily executed.", "success");
        createMessage = CreateMessage;
    });
});

var Page = React.createClass({
    render: function () {
        return (
            <div>
                <div className="col-md-6">
                    <div className="panel panel-default">
                        <div className="panel-heading">Роли</div>
                        <div className="panel-body">
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Название роли</th>
                                        <th>Кол-во пользователей</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {Object.entries(this.props.roles).map(function (val, i) {
                                        var id = "role_" + val[0];
                                        return (
                                            <tr key={i} id={id}>
                                                <td>{val[0]}</td>
                                                <td>{val[1]}</td>
                                            </tr>
                                            );
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 hidden-sm hidden-xs">
                    <div className="panel panel-default time">
                        <div className="panel-heading">Время</div>
                        <iframe className="clock" src="http://free.timeanddate.com/clock/i5ixgflb/n166/tlru33/fs36/ftb/tt0/tw0/tm3/td2/th1/ts1/tb4" frameBorder="0" width="183" height="86"></iframe>
                    </div>
                </div>

                <div className="col-md-12">
                    <div className="panel panel-default">
                        <div className="panel-heading">Пользователи</div>
                        <div className="panel-body">
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Е-Мейлы</th>
                                        <th>Роли</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {Object.entries(this.props.users).map(function(val, i) {
                                        var id = val[0].substr(0, 4);
                                        return (
                                        <tr id={id} key={i}>
                                            <td>{val[0]}</td>
                                            <td>{val[1]}</td>
                                            <td>
                                                <a className="btn btn-info pull-right" onClick={() => showMenu(val[0])} role="button" style={{marginLeft: 12 + 'px'}}>Изменить</a>
                                                <a className="btn btn-danger pull-right" onClick={() => removeUserDialog(val[0])} role="button">Удалить</a>
                                            </td>
                                        </tr>
                                            );
                                        }
                                    )
                                  }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            );
    }
});

refreshPage();

function refreshPage() {
    var mainJson = $.getJSON("/Admin/GetBodyContent");

    mainJson.done(function (data) {
        console.log(data);
        ReactDOM.render(
            <Page users={data.users} roles={data.roles}></Page>, 
document.getElementById("MainPage")
        );
});
}


var Form = React.createClass({
    render: function () {
        return (
            <div className="change-user-form">
                { 
                    Object.entries(this.props.userRoles).map(function (key, i) {
                        console.log(key);
                        return (
                            <div className="checkbox"  key={ i }>
                            <label><input type="checkbox" id={key[0]} defaultChecked={ key[1] } className="pull-left" />{ key[0] }</label>
                            </div>
                            );
                    })
                }
                <button className="btn btn-primary" id="submit">Изменить</button>
            </div>
            );
                }
                });

var Menu = React.createClass({
    render: function () {
        return (
            <div className="panel panel-primary edit-user">
                <div className="panel-heading">
                { this.props.name }
                    <span className="pull-right clickable" ><i className="fa fa-times"></i></span>
                </div>
                { this.props.children}
            </div>
            );
    }
});

var Dialog = React.createClass({
    render: function () {
        return (
            <div className="change-user-form">
                <div>{ this.props.message }</div>
                <div className="buttons">
                <div className="btn btn-success pull-left"  id="confirmAccept" style={{marginBottom: 10+"px"}}>Удалить</div>
                <div className="btn btn-danger pull-right" id="confirmDeceline">Отменить</div>
                </div>
            </div>
            );
    }
});
function sendForm(newEmail, oldEmail, privelegies) {
    var json = $.post("/Admin/ChangeUser", {
        newEmail: newEmail,
        oldEmail: oldEmail,
        privelegies: privelegies
    }, function (data) {
        console.log(data);
            if (data == "ok") {
                closePanel();
                createMessage("Изменения сохранены!", "success");
                refreshPage();
            }
            else {

                createMessage("Произошла ошибка!", "danger");
            }
        
    });
}

function closePanel() {
    console.log("Panel closing...");
    $(".edit-user").slideUp(500, function () {
        $(".edit-user").remove();
    });
    
}

function showMenu(user) {
    var json = $.getJSON("/Admin/EditUser?user=" + user);
    json.done(function (data) {
        console.log(data);
        ReactDOM.render(
        <Menu name={ user }>
            <Form userRoles={data.userRolesChecked }></Form>
        </Menu>,
        document.getElementById("EditUsers")
        );
        $(".edit-user").fadeIn(600, function () { 
            $(".clickable").on('click', function () {
                closePanel();
            });
        });
        $("#submit").click(function () {
            var newEmail = user;
            var oldEmail = user;
            
            console.log($("#User").prop('checked'));
            var privelegies = {
                Manager: $("#Manager").prop('checked'),
                User: $("#User").prop('checked'),
                Redactor: $("#Redactor").prop('checked'),
                Administrator: $("#Administrator").prop('checked')
            };
            sendForm(newEmail, oldEmail, privelegies);
        });
    });
}

function removeUserDialog(user) {
    var message = "Вы действительно хотите удалить пользователя " + user + "?";
    ReactDOM.render(
        <Menu name={ user }>
            <Dialog message={ message }></Dialog>
        </Menu>,
        document.getElementById("EditUsers"));
    $(".edit-user").fadeIn(600, function () {
        $(".clickable").on('click', function () {
            closePanel();
        });
    });
    $("#confirmAccept").click(function () {
        var response = $.getJSON("/Admin/RemoveUser?user=" + user);
        response.done(function (data) {
            console.log(data);
        if (data.result == "ok") {
            closePanel();
            createMessage("Пользователь успешно удален!", "success");
            refreshPage();
        }
        else {
            console.log(data.err);
            createMessage("Произошла ошибка!", "danger");
        }
        });
    });
    $("#confirmDeceline").click(function () {
        closePanel();
    });
}