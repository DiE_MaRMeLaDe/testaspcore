﻿var messageCounter = 0;

function CreateMessage(message, messageType) {
    var className = '"alert alert-' + messageType + '"';
    var id = "message" + messageCounter;
    var div = document.createElement('div');
    div.id = id;
    div.innerHTML = "<div class="+className+">" + message + "</div>";
    document.getElementById("messageContainer").appendChild(div);
    $("#" + id).css("margin-left", "100%");
    $("#" + id).animate({
        marginLeft: "0%"
    }, 1000);
    setTimeout(function () {
        $("#" + id).animate({
            marginTop: "-72px"
        }, 500, function () {
            $("#" + id).remove();
        });
    }, 3000);
    messageCounter++;
}