﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Test_Auth.Models
{
    public class UserArticles
    {
        public string UserKey { get; set; }
        [Key]
        public string ArticleKey { get; set; }

        [ForeignKey("UserKey")]
        public ApplicationUser User { get; set; }
        [ForeignKey("ArticleKey")]
        public Article Article { get; set; }
    }
}
