﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test_Auth.Models.HomeViewModels
{
    public class HomeIndexViewModel
    {
        public ICollection<Article> Articles { get; set; }
    }
}
