﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Test_Auth.Models
{
    public class Article
    {
        public string UserName { get; set; }
        public virtual ApplicationUser Creator { get; set; }
        public string Content { get; set; }
        public string ShortContent { get; set; }
        public int Views { get; set; }
        public string Header { get; set; }
        [Key]
        public string Key { get; set; }
        public DateTime Date { get; set; }

        public override string ToString()
        {
            return $"User: {UserName}, Header: {Header}, Content: {Content}, Date: {Date.ToString()}";
        }
    }
}
