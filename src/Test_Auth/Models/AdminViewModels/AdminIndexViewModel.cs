﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Test_Auth.Models.AdminViewModels
{
    public class AdminIndexViewModel
    {
        public Dictionary<ApplicationUser, string> Users { get; set; }
        public Dictionary<IdentityRole, int> Roles { get; set; }
    }
}
