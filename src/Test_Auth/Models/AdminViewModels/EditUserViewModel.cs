﻿using System.Collections.Generic;

namespace Test_Auth.Models.AdminViewModels
{
    public class EditUserViewModel
    {
        public Dictionary<string, string> UserRolesChecked { get; set; }
        public ApplicationUser User { get; set; }
    }
}
