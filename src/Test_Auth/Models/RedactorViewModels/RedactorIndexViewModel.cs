﻿namespace Test_Auth.Models.RedactorViewModels
{
    public class RedactorIndexViewModel
    {
        public Article Article { get; set; }
    }
}
