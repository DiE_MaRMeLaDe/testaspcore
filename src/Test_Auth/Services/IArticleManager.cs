﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test_Auth.Models;

namespace Test_Auth.Services
{
    public interface IArticleManager
    {
        Task<List<Article>> GetUserArticles(ApplicationUser user);
        Task<List<Article>> ArticlesToList();
        Task<ArticleManager.AmResult> CreateArticle(ApplicationUser user, Article article);
        Task<ArticleManager.AmResult> ChangeArticle(Article article, Article newArticle);
        Task<ArticleManager.AmResult> DeleteArticle(Article article);
    }
}
