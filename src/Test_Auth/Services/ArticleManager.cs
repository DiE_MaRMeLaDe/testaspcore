﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Test_Auth.Data;
using Test_Auth.Models;
using Test_Auth.Services;

namespace Test_Auth.Services
{
    public class ArticleManager : IArticleManager
    {
        public enum AmResult
        {
            OK = 1,
            BAD = 0
        }
        private ApplicationDbContext _context;
        public ArticleManager(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<List<Article>> GetUserArticles(ApplicationUser user)
        {
            return await _context.Articles.Where(a => a.Creator == user).ToListAsync();
        }

        public async Task<List<Article>> ArticlesToList()
        {
            return await _context.Articles.ToListAsync();
        }

        public async Task<AmResult> CreateArticle(ApplicationUser user, Article article)
        {
            if (user.Email == null || article.Content == null || article.Header == null)
            {
                return AmResult.BAD;
            }
            else
            {
                article.Creator = user;
                article.Date = DateTime.Now;
                article.Key = generateKey();
                _context.Articles.Add(article);
                _context.UserArticles.Add(new UserArticles {
                    ArticleKey = article.Key,
                    UserKey = user.Id
                });
                await _context.SaveChangesAsync();
                return AmResult.OK;
            }
        }
        public async Task<AmResult> ChangeArticle(Article article, Article newArticle)
        {
            try
            {
                var Article = await _context.Articles.FirstAsync(a => a == article);
                Article = newArticle;
                await _context.SaveChangesAsync();
                return AmResult.OK;
            }
            catch
            {
                return AmResult.BAD;
            }
        }

        public async Task<AmResult> DeleteArticle(Article article)
        {
            try
            {
                var articleToDel = await _context.Articles.FirstAsync(a => a == article);
                _context.Articles.Remove(articleToDel);
                _context.UserArticles.Remove(await 
                    _context.UserArticles.Where(
                        a => a.ArticleKey == article.Key
                        )
                    .FirstAsync());
                await _context.SaveChangesAsync();
                return AmResult.OK;
            }  
            catch
            {
                return AmResult.BAD;
            }
        }


        private string generateKey()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random random = new Random();
            return new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
